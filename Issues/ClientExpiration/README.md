
# Client Expiration and Revival

This documentation elaborates on the event of IBC client expiration and how to revive the expired client.

## The cause of client expiration

Opening an IBC channel requires two clients created and used for consensus state verification.

For example, 
- Terra Chain requires `Oppy Chain Client`.
- Oppy Chain requires `Terra Chain Client`.

`Client expiration` will result in the invalidation of the created channel.

When the relayer (e.g., launched with `hermes` tool) is actively running with `refresh = true`, 
the relayer could automatically refresh the client consensus state as handling users' relaying messages or transactions.
Moreover, the value of `trusting period` (which is normally 2/3 of the unbonding time) set in the `chain` filed of `config.toml` file will be
used to measure whether the client is expired or not.
In details, the client will expire when the time difference between its consensus state and the chain's current state exceeds pre-defined `trusting period`.


## Expired Client Revival

Basically, reviving the expired client (or frozen client) requires the relayer to send a `update-client` proposal
to the chain and get the proposal passed to activate the expired client again. 
The entire process can be break down into several steps shown below:
1. Creating substitute client (before submission)
2. Preparing deposit / title / description for `update-client` proposal (before submission)
3. Submitting `update-client` proposal
4. (optional) Voting for the proposal
5. (optional) Testing the revived channel

### 1. Creating substitute client
The user first need to create a substitute client based on the **exactly same** configuration as the expired client.
Otherwise, the transaction of submitting `update-client` proposal will not be executed and report
the error *"message index:subject client state does not match substitute client state: invalid client state substitute"*

!!! Moreover, please make sure that **"the substitute client are continually updated during the voting period"**.

Once you create the substitute client, you can check its status with the following command:
- `client-id`: the identified id (e.g., 07-tendermint-0)
```
oppyChaind query ibc client status {client-id}
```

### 2. Preparing deposit/title/description
The user also needs to address three things in the `update-client` proposal.
- `--deposit string`: deposit of proposal
- `--title string`: title of proposal
- `--description string`: description of proposal

The `title` and `description` are very straight forward. 
About the `deposit`, submitting the proposal requires the user to deposit required amount of asset.
The amount of deposited asset must be greater and equal to the minimum deposit. 
To fulfill the requirement based on the chain setting, the user can first check
the required minimum deposit with the following command:
```
oppyChaind query gov params
```

The output result might be like:
```text
deposit_params:
  max_deposit_period: "172800000000000"
  min_deposit:
  - amount: "10000000"
    denom: poppy
tally_params:
  quorum: "0.334000000000000000"
  threshold: "0.500000000000000000"
  veto_threshold: "0.334000000000000000"
voting_params:
  voting_period: "60000000000"
```


### 3. Submitting the proposal
Once the user has the **substitute client** and required material, the user can submit the proposal 
based on the command below:
- `gov submit-proposal` (transaction type)
- `update-client` (proposal type)
- `07-tendermint-0` (expired client id)
- `07-tendermint-1` (new or active client id)
- `--deposit 100000000000poppy` (deposit coins for your proposal)
- `--title "replace the expired client"` (the title of the proposal)
- `--description "Relacing expired ibc client with the active client"` (description)
```
oppyChaind tx gov submit-proposal update-client 07-tendermint-0 07-tendermint-1 
    \--keyring-backend file --home .oppyChaind/ 
    \--chain-id oppyChain-1 
    \--gas auto 
    \--gas-prices 0poppy 
    \--gas-adjustment 1.5  
    \--from operator 
    \--title "replace the expired client"
    \--deposit 100000000000poppy 
    \--description “replacing expired abc client with new client”
```

The user can check their proposal based on the command below:
- `proposal id`: the proposal id (e.g., `1`)

```shell
oppyChaind query gov proposal {proposal id}
```

## 4. (optional) Voting for the proposal
The user can also vote for the proposal by running the command below:
- `proposal id`: the proposal id (e.g., `1`)
- `option`: `yes`/`no`/`no_with_veto`/`abstain`

```shell
oppyChaind tx gov vote {proposal id} {option} 
    \--keyring-backend test 
    \--home /root/.oppyChain/ 
    \--chain-id oppyChain-C 
    \--gas auto 
    \--gas-prices 0poppy 
    \--gas-adjustment 1.5 
    \--from operator
```

Alternatively, you can do `weighted-vote`:
```shell
oppyChaind tx gov weighted-vote {proposal id} {option} 
```

## 5. (optional) Testing the revived channel
- `port`: ibc port (e.g., transfer)
- `channel`: ibc channel id (e.g., channel-0)
- `amount`: 99999poppy
```shell
oppyChaind tx ibc-transfer transfer {port} {channel} {receiver address} {amount} 
 \--keyring-backend test 
 \--home /root/.oppyChain/ 
 \--chain-id oppyChain-A 
 \--gas auto 
 \--gas-prices 0poppy 
 \--gas-adjustment 1.5 
 \--from operator
```

## Reference

### Cosmos Documentation for submitting a proposal
[Cosmos Doc](https://ibc.cosmos.network/main/ibc/proposals.html)

### Terra IBC Incident
[The Proposal](https://forum.cosmos.network/t/proposal-terraclassic-ibc-reactivation-via-client-unfreeze/7927)

### IBC Client Update Proposal (Terra Classic <--> Kava)

[Kava Proposal 109](https://www.mintscan.io/kava/proposals/109)

