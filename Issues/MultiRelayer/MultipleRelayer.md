# Multiple Relayers

## Multiple Relayer Operator Address

---

Relayer 1

- juno1hnqw6tkk6h05q866rdpfkc86l7fd8upjq7s62d
- osmo1hnqw6tkk6h05q866rdpfkc86l7fd8upj7hq3mr

Relayer 2

- juno1gmt9nurxq85sjlhjzrmzwdvqy7ea0w0a86x8fq
- osmo1gmt9nurxq85sjlhjzrmzwdvqy7ea0w0aenkvcw

Test Account

- juno1hnqw6tkk6h05q866rdpfkc86l7fd8upjq7s62d
- osmo1hnqw6tkk6h05q866rdpfkc86l7fd8upj7hq3mr

### Two relayers listening to the same channel

![Untitled](figs/Untitled.png)

![Untitled](figs/Untitled 1.png)

![Untitled](figs/Untitled 2.png)

## IBC transfer from Osmo to Juno

---

channel-369

|  | Amount | IBC transfer | IBC receive | IBC acknowledge |
| --- | --- | --- | --- | --- |
| 1 | 2.333 osmo | relayer 1 | relayer 2 | relayer 2 |
| 2 | 1 osmo | relayer 1 | relayer 2 | relayer 2 |
| 3 | 0.1 osmo | relayer 1 | relayer 1 | relayer 2 |
| 4 | 0.2  | relayer 1 | relayer 2 | relayer 2 |

## One IBC transfer from Juno to Osmo

---

channel 83

|  | Amount | IBC transfer | IBC receive | IBC acknowledge |
| --- | --- | --- | --- | --- |
| 1 | 0.1 juno | relayer 1 | relayer 2 | relayer 2 |
| 2 | 0.2 juno | relayer 1 | relayer 1 | relayer 2 |
| 3 | 0.3 juno | relayer 1 | relayer 2 | relayer 2 |

### (OSMO → JUNO) Transaction One

---

> amount: 2.333 Osmo
(**Relayer 1**) IBC transfer
(**Relayer 2**) IBC ack
(**Relayer 2**) IBC receive
> 

relayer1

![Untitled](figs/Untitled 3.png)

relayer2

![Untitled](figs/Untitled 4.png)

Transaction Status

![Untitled](figs/Untitled 5.png)

![Untitled](figs/Untitled 6.png)

### (OSMO → JUNO) Transaction Three

---

> amount: 0.1 osmo
(Relayer 1) IBC transfer
(**Relayer 1**) IBC receive
(Relayer 2) IBC ack
> 

![Untitled](figs/Untitled 7.png)

![Untitled](figs/Untitled 8.png)

### Transactions on OSMO side

---

> **Relayer 1** handled all **IBC transfer
Relayer 2** handled all **IBC ACK**
> 

![Untitled](figs/Untitled 9.png)

### (JUNO → OSMO) Transaction One

---

> (Relayer 1) IBC transfer
(Relayer 2) IBC Receive
(Relayer 2) IBC ACK
> 

relayer 1

![Untitled](figs/Untitled 10.png)

relayer 2

![Untitled](figs/Untitled 11.png)

IBC transfer

![Untitled](figs/Untitled 12.png)

IBC receive

![Untitled](figs/Untitled 13.png)

IBC ack

![Untitled](figs/Untitled 14.png)
