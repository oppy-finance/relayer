#IBC Terra Classic

Tax Rate: (tax) 0.2% Gas Amount (G): ~100,000

Default Gas Price (P): ~10

Transfer Amont (A): xxx (in lunc)
```shell
Price = (A*tax*1000000 + G*P) / G
```

###Example
```shell
A = 1000 Lunc
Price = (1,000 * 0.2% *1,000,000 + 100,000 * 10) / 100,000 = 20 + 10 = 30
```



## Terra Station Transfer
https://finder.terra.money/classic/tx/9CB66CE702A07913DE5D8E78100B5CCFA68D17AA8A3769D4E5334AD5E6A816CF

### Example (transfer 100 Lunc in terra)
```shell
Transfer Amount >>  100 Lunc
Transaction Fee >> 1.096521 Lunc
Tax >> 0.2 Lunc
Transaction Fee (exclude tax) >> 0.896521 Lunc -> 896521 uluna
Gas Used >> 96,008
```
**Gas Price: 896,521 / 96,008 = 9.338**

### Example (transfer 1000 Lunc in terra)
```shell
Transfer Amount: 1000 Lunc
Transaction Fee: 2.896521 Lunc
Tax: 2 Lunc
Transaction Fee (exclude tax): 0.896521 Lunc -> 896521 uluna
Gas Used: 95,694
```
**Gas Price: 896521 / 95694 = 9.36**



## Case 0 (Terra Station Wallet, normal transfer)

```shell
Transfer Amount: 100
Gas Amount Provided: 123,410
Gas Used: 95,717
Transaction Fee: 1.851150 Lunc
```


## Case 1 (Keplr, normal transfer)
- adjust gas price
- not adjust gas amount

### Transfer Info
```shell
Transfer Amount: 100
Gas Wanted: 80233
Gas Used: 83536
```

### log
```log
Fail to send token: out of gas in location: WritePerByte; 
gasWanted: 80233, gasUsed: 83536: out of gas
```


## Case 2 (Keplr, normal transfer)

### Transfer Info
```shell
Transfer Amount: 15,000 LUNC
Transaction Fee Provided: 1203885uluna
Transaction Fee Required: 454520uluna + 30000000uluna (txn fee + tax)
```

### Log
```log
Fail to send token: insufficient fees; 
got: "1203495uluna", 
required: "15245uaud,15245ucad,11233uchf,78629ucny,72210udkk,10030ueur,8826ugbp,93873uhkd,174907940uidr,872936uinr,1313415ujpy,13621157ukrw,30454520uluna,34385538umnt,48140umyr,100292unok,609771uphp,8420usdr,100292usek,16047usgd,370677uthb,320932utwd,12035uusd" 
= "15245uaud,15245ucad,11233uchf,78629ucny,72210udkk,10030ueur,8826ugbp,93873uhkd,174907940uidr,872936uinr,1313415ujpy,13621157ukrw,454520uluna,34385538umnt,48140umyr,100292unok,609771uphp,8420usdr,100292usek,16047usgd,370677uthb,320932utwd,12035uusd"(gas) +"30000000uluna"(stability): insufficient fee
```


## Case 3 (Keplr, IBC transfer)

### Transfer Info
```shell
Transfer Amount: 1,000 Lunc
Transaction Fee: 3.044370 Lunc
Gas Used: 78,860
```

### Transaction Link
https://finder.terra.money/classic/tx/C88A68B4A580B6620315A2A28702C0EDAB1E393702C1974213CD8D5C4F193344

