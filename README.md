# Hermes Relayer Setup


## 0. Install hermes

hermes official site: https://hermes.informal.systems/installation.html

hermes github releases: https://hermes.informal.systems/installation.html


```
# 1. Make the directory where we'll place the binary:
mkdir -p $HOME/.hermes/bin

# 2. Download and extract the corresponding binary file
wget https://x.x.x/xxx.tar
tar -C $HOME/.hermes/bin/ -vxzf $ARCHIVE_NAME

# 3. Update your your `~/.bashrc` or '~/.zshrc' by adding the following line
export PATH="$HOME/.hermes/bin:$PATH"

# 4. Source your `~/.bashrc` or '~/.zshrc' file
source ~/.bashrc

```


## 1. Import key for relayer with mnemonic seeds
```
 # hermes keys add --key-name [keyname] --chain [chain_id] --mnemonic-file [mnemonic_file]

hermes keys add --key-name osmo-relayer --chain osmo-teset-4 --mnemonic-file [mnemonic_file]
```

## 2. Setup _config.toml_ filr for hermes relayer
[template](https://hermes.informal.systems/example-config.html)

```
# the most important value for relayed chains
# here is the example for juno testnet
# more details in 'config.toml' file

id = 'osmo-test-4'
rpc_addr = 'http://143.198.139.33:26657'
grpc_addr = 'http://143.198.139.33:9090'
websocket_addr = 'ws://143.198.139.33:26657/websocket'
...
account_prefix = 'osmo'
key_name = 'osmo-relayer'
...
gas_price = { price = 0.1, denom = 'uosmo' }
gas_multiplier = 1.2
...

```
## 3. Create new channel between two chains
```
# create new IBC channel for two chains
# hermes create channel --a-chain [chainID0] --b-chain [chainID1] --a-port [portName] --b-port [portName] --new-client-connection

hermes create channel --a-chain oppyChain-1 --b-chain osmo-test-4 --a-port transfer --b-port transfer --new-client-connection

```


## 4. Start the relayer
```
hermes --config [configPath] start
```

# Multiple Relayer Operator Address

Multiple relayers can work for one opened IBC channel.

## Settings
```
- Two machines with same clients and connections
- Same Channel opened in both machines
- Two machines with different keys to launch different relayers
- Hermes Start
```

# Chain Configuration

## Terra Classic (mainnet)

* Info
  * `chain id`: columbus-5
  * `account prefix`: terra
  * `denom`: Lunc
  * `minimal denom`: uluna
  * `decimal`: 6
  * `rpc url (from official site)`: https://terra-rpc.easy2stake.com
  * `rest url (from official site)`: https://lcd.terra.dev
  * `rpc_addr`: http://x.x.x.x:26657
  * `grpc_addr`: http://x.x.x.x:9090
  * More reference: [RPC and LCD endpoints](https://classic-docs.terra.money/docs/develop/endpoints.html?highlight=rpc)


## Juno (testnet)

* Info
  * `chain id`: uni-3
  * `account prefix`: terra
  * `denom`: JUNO
  * `minimal denom`: ujunox
  * `decimal`: 
  * `rest url`: 
  * `rpc_addr`: http://135.181.59.162:26657
  * `grpc_addr`: http://135.181.59.162:26090
  * `websocket_addr`: ws://135.181.59.162:26657/websocket

* Faucet
  * Through [Discord Channel](https://discord.com/invite/juno)

* Add to Keplr Wallet
  * Through site https://uni.juno.omniflix.co/

## Osmosis (testnet)

* Info
  * `chain id`: osmo-test-4
  * `account prefix`: osmo
  * `denom`: OSMO
  * `minimal denom`: uosmo
  * `decimal`: 
  * `rest url`:
  * `rpc_addr`: http://143.198.139.33:26657
  * `grpc_addr`: http://143.198.139.33:9090
  * `websocket_addr`: ws://143.198.139.33:26657/websocket

* Faucet
    * Through site [link](https://faucet.osmosis.zone/#/)


# Useful Links

### Relayer Explorer
* [mainnet](https://www.mintscan.io/cosmos/relayers)
* [testnet](https://testnet.mintscan.io/osmosis-testnet/relayers)
* [cosmos directory](https://cosmos.directory/) (RPC, Rest, gRPC info)
* [polkachu](https://polkachu.com/) (API, rpc, rest endpoint info; snapshot downloading)
* [chainlayer](https://www.chainlayer.io/) (snapshot downloading)